var CronJob = require('cron').CronJob;
var bot = require('./bot.js');
var callBot = function() {
  console.log("Bot Start....")
  bot.start();
}

var health_check = function() {
  console.log("OK");
}

new CronJob({
  cronTime: "00 31 * * * *", // Asia bangkok everyday, 11:59 AM
  onTick: callBot,
  start: true,
  timeZone: "Asia/Bangkok"
});

new CronJob({
  cronTime: "10 * * * * *", // Asia bangkok everyday, 11:59 AM
  onTick: health_check,
  start: true,
  timeZone: "Asia/Bangkok"
});
