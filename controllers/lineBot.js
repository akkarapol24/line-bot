const request = require('request');
var Promise = require('promise');
const LINE_MESSAGING_API = 'https://api.line.me/v2/bot/message';
const LINE_HEADER = {
    'Content-Type': 'application/json',
    'Authorization': ``
};
const rp = require('request-promise');
const $ = require('cheerio');
let reply_token = '';
var redisClient = require('redis').createClient(process.env.REDIS_URL);
let key_cache = '';
let data_covid = ''

const getWebhook = async(req, res, next) => {
    try {
        reply_token = req.body.events[0].replyToken;
        let msg = req.body.events[0].message.text;
        let reply_msg = '';

        if (msg == 'hello') {
            reply_msg = 'Hi';
            reply(reply_token, reply_msg);
        } else {
            const request_promise = require('request-promise');
            const util = require('util');
            const setTimeoutPromise = util.promisify(setTimeout);

            setTimeoutPromise(10000).then((value) => {
                request_promise({
                    method: `POST`,
                    uri: `${LINE_MESSAGING_API}/push`,
                    headers: LINE_HEADER,
                    body: JSON.stringify({
                        to: config.owner_id,
                        messages: [{
                            type: "text",
                            text: `${msg}`
                        }]
                    })
                });
            });
        }
        res.sendStatus(200);
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'description': 'something went wrong, Please try again'
        });
    }
}

const createNews = async(req, res, next) => {
    try {
        let msg = await getNews();
        redisClient.get(key_cache, async(error, data) => {
            if (error) {
                res.json({
                    message: 'Something went wrong!',
                    error
                });
            }


            let groups = ["xsdfdsfghsgfjsjhfgsdgfh"]
            push_multicast(groups, msg).then(function() {
                redisClient.setex(key_cache, 6500, JSON.stringify(msg));
                res.sendStatus(200)
            });
        });
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            'code': 'SERVER_ERROR',
            'description': 'something went wrong, Please try again'
        });
    }
}

async function getNews() {
    let data_total = ''
    let data_normal = ''
    let data_treat = ''
    let data_deaded = ''
    let data_update = ''
    await rp("https://covid19.th-stat.com")
        .then(function(html) {
            //success!
            data_update = $('.col-xs-12.col-sm-6.col-lg-5.offset-lg-1 > .block-title-page.hidden-xs.hidden-sm > h2', html).text()
            data_covid = html
        })
        .catch(function(err) {
            //handle error
        });

    data_update = data_update.split(' ');
    key_cache = data_update[2].replace('/', '').replace('/', '') + data_update[3].replace(':', '')

    data_total = split_text('data_total')
    data_normal = split_text('data_normal')
    data_treat = split_text('data_treat')
    data_deaded = split_text('data_deaded')
    console.log(data_total)
    console.log(data_normal)
    console.log(data_treat)
    console.log(data_deaded)

    let current_date = await convert_date_th(data_update[2] + ' ' + data_update[3]);
    console.log(current_date)
    let msg = await gen_message(current_date, data_total, data_normal, data_treat, data_deaded);

    return msg
}

async function gen_message(current_date, data_total, data_normal, data_treat, data_deaded) {

    content_total = []
    if (data_total.total) {
        content_total.push({
            "type": "text",
            "text": "ติดเชื้อสะสม",
            "size": "md",
            "color": "#ffffff",
            "style": "normal",
            "weight": "bold",
            "align": "center"
        });
        content_total.push({
            "type": "text",
            "text": data_total.total,
            "size": "xl",
            "color": "#ffffff",
            "style": "normal",
            "weight": "bold",
            "align": "center"
        });
    }

    if (data_total.progress) {
        content_total.push({
            "type": "text",
            "text": data_total.progress,
            "size": "md",
            "color": "#ffffff",
            "style": "normal",
            "weight": "bold",
            "align": "center"
        });
    }

    content_normal = []
    if (data_normal.total) {
        content_normal.push({
            "type": "text",
            "text": "หายแล้ว",
            "size": "md",
            "color": "#ffffff",
            "style": "normal",
            "weight": "bold",
            "align": "center"
        });
        content_normal.push({
            "type": "text",
            "text": data_normal.total,
            "size": "xl",
            "color": "#ffffff",
            "style": "normal",
            "weight": "bold",
            "align": "center"
        });
    }

    if (data_normal.progress) {
        content_normal.push({
            "type": "text",
            "text": data_normal.progress,
            "size": "md",
            "color": "#ffffff",
            "style": "normal",
            "weight": "bold",
            "align": "center"
        });
    }

    content_treat = []
    if (data_treat.total) {
        content_treat.push({
            "type": "text",
            "text": "รักษาอยู่ใน รพ.",
            "size": "md",
            "color": "#ffffff",
            "style": "normal",
            "weight": "bold",
            "align": "center"
        });
        content_treat.push({
            "type": "text",
            "text": data_treat.total,
            "size": "xl",
            "color": "#ffffff",
            "style": "normal",
            "weight": "bold",
            "align": "center"
        });
    }

    if (data_treat.progress) {
        content_treat.push({
            "type": "text",
            "text": data_treat.progress,
            "size": "md",
            "color": "#ffffff",
            "style": "normal",
            "weight": "bold",
            "align": "center"
        });
    }

    content_deaded = []
    if (data_deaded.total) {
        content_deaded.push({
            "type": "text",
            "text": "เสียชีวิต",
            "size": "md",
            "color": "#ffffff",
            "style": "normal",
            "weight": "bold",
            "align": "center"
        });
        content_deaded.push({
            "type": "text",
            "text": data_deaded.total,
            "size": "xl",
            "color": "#ffffff",
            "style": "normal",
            "weight": "bold",
            "align": "center"
        });
    }

    if (data_deaded.progress) {
        content_deaded.push({
            "type": "text",
            "text": data_deaded.progress,
            "size": "md",
            "color": "#ffffff",
            "style": "normal",
            "weight": "bold",
            "align": "center"
        });
    }

    flex_msg = {
        "type": "bubble",
        "body": {
            "type": "box",
            "layout": "vertical",
            "contents": [{
                    "type": "text",
                    "text": "รายงานสถานการณ์ โควิด-19",
                    "weight": "bold",
                    "color": "#1DB446",
                    "size": "lg"
                },
                {
                    "type": "text",
                    "text": `อัพเดทข้อมูลล่าสุด ${current_date}`,
                    "size": "sm",
                    "margin": "xs",
                    "style": "normal"
                },
                {
                    "type": "separator",
                    "margin": "xxl"
                },
                {
                    "type": "box",
                    "layout": "vertical",
                    "margin": "xxl",
                    "spacing": "sm",
                    "contents": [{
                            "type": "box",
                            "layout": "vertical",
                            "contents": content_total,
                            "backgroundColor": "#e1298e",
                            "spacing": "none",
                            "margin": "none",
                            "cornerRadius": "sm",
                            "paddingAll": "2px"
                        },
                        {
                            "type": "separator",
                            "margin": "md"
                        },
                        {
                            "type": "box",
                            "layout": "vertical",
                            "contents": content_normal,
                            "backgroundColor": "#046034",
                            "spacing": "none",
                            "margin": "none",
                            "cornerRadius": "sm",
                            "paddingAll": "2px"
                        },
                        {
                            "type": "separator",
                            "margin": "md"
                        },
                        {
                            "type": "box",
                            "layout": "vertical",
                            "contents": content_treat,
                            "backgroundColor": "#179c9b",
                            "spacing": "none",
                            "margin": "none",
                            "cornerRadius": "sm",
                            "paddingAll": "2px"
                        },
                        {
                            "type": "separator",
                            "margin": "md"
                        },
                        {
                            "type": "box",
                            "layout": "vertical",
                            "contents": content_deaded,
                            "backgroundColor": "#666666",
                            "spacing": "none",
                            "margin": "none",
                            "cornerRadius": "sm",
                            "paddingAll": "2px"
                        },
                        {
                            "type": "separator",
                            "margin": "xxl"
                        }
                    ]
                }
            ]
        },
        "styles": {
            "footer": {
                "separator": true
            }
        }
    }

    return flex_msg
}

async function convert_date_th(current_date) {
    current_date = current_date.split(' ')
    date = current_date[0].split('/')
    let month_th = ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."];
    let date_th = date[0] + ' ' + month_th[parseInt(date[1]) - 1] + ' ' + (parseInt(date[2]) + 543) + ' ' + current_date[1]
    return date_th
}

async function push_multicast(groups, msg) {
    await Promise.all(
        groups.map(async group => {
            await push_noti(group, msg);
        })
    )
}

function push_noti(group, msg) {
    return new Promise(function(resolve, reject) {
        request.post({
            url: `${LINE_MESSAGING_API}/push`,
            headers: LINE_HEADER,
            body: JSON.stringify({
                to: group,
                messages: [{
                    type: "flex",
                    altText: "รายงานสถานการณ์ โควิด-19",
                    contents: msg
                }]
            })
        }, (err, res, body) => {
            // console.log(body)
            if (res) {
                resolve(res);
                console.log(res)
            }
            if (err) {
                reject(err);
                console.log(err)
            }
        });
    });
}

function split_text(type) {
    let data_total = {}
    if (type == 'data_total') {
        data_total.total = $('.col-xs-12.col-sm-6.col-lg-6.pd-0 > .block-st-all > .col-xs-12.col-sm-12 > .card-default > h1', data_covid).text().trim()
        data_total.progress = $('.col-xs-12.col-sm-6.col-lg-6.pd-0 > .block-st-all > .col-xs-12.col-sm-12 > .card-default > span', data_covid).text().trim()
    } else if (type == 'data_normal') {
        data_total.total = $('.col-xs-12.col-sm-6.col-lg-6.pd-0 > .block-st-all > .col-xs-4.col-sm-4:nth-child(2) > .card-default > h1', data_covid).text().trim()
        data_total.progress = $('.col-xs-12.col-sm-6.col-lg-6.pd-0 > .block-st-all > .col-xs-4.col-sm-4:nth-child(2) > .card-default > span', data_covid).text().trim()
    } else if (type == 'data_treat') {
        data_total.total = $('.col-xs-12.col-sm-6.col-lg-6.pd-0 > .block-st-all > .col-xs-4.col-sm-4:nth-child(3) > .card-default > h1', data_covid).text().trim();
        data_total.progress = $('.col-xs-12.col-sm-6.col-lg-6.pd-0 > .block-st-all > .col-xs-4.col-sm-4:nth-child(3) > .card-default > span', data_covid).text().trim();
    } else if (type == 'data_deaded') {
        data_total.total = $('.col-xs-12.col-sm-6.col-lg-6.pd-0 > .block-st-all > .col-xs-4.col-sm-4:nth-child(4) > .card-default > h1', data_covid).text().trim();
        data_total.progress = $('.col-xs-12.col-sm-6.col-lg-6.pd-0 > .block-st-all > .col-xs-4.col-sm-4:nth-child(4) > .card-default > span', data_covid).text().trim();
    }

    return data_total
}

function reply(reply_token, msg) {
    let body = JSON.stringify({
        replyToken: reply_token,
        messages: [{
            type: 'text',
            text: msg
        }]
    })
    request.post({
        url: `${LINE_MESSAGING_API}/reply`,
        headers: LINE_HEADER,
        body: body
    }, (err, res, body) => {
        console.log(res);
        console.log('status = ' + res.statusCode);
    });
}

module.exports = {
    getWebhook: getWebhook,
    createNews: createNews
}