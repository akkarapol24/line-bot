const express = require('express')
const app = express()
const path = require('path')
const PORT = process.env.PORT || 5000
const lineBotController = require('./controllers/lineBot');

app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(express.static(path.join(__dirname, 'public')))
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')
app.get('/', (req, res) => res.render('pages/index'))
app.get('/create-news', lineBotController.createNews)
app.get('/cool', (req, res) => res.sendStatus(200))
app.listen(PORT, () => console.log(`Listening on ${ PORT }`))